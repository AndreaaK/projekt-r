package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.MozeBiti;

@Dao
public interface MozeBitiDao {
    @Insert
    void insertMozeBiti(MozeBiti mozeBiti);
}
