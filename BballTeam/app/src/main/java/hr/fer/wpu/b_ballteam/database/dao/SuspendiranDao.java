package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.Suspendiran;

@Dao
public interface SuspendiranDao {
    @Insert
    void insertSuspendiran(Suspendiran suspendiran);
}
