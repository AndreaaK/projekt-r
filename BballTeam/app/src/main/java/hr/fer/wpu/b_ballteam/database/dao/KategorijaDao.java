package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.PlayerListClass;
import hr.fer.wpu.b_ballteam.database.models.Igrac;
import hr.fer.wpu.b_ballteam.database.models.Kategorija;

@Dao
public interface KategorijaDao {
    @Insert
    void insertKategorija(Kategorija kategorija);

    @Query("SELECT IDKategorija FROM Kategorija WHERE NazivKategorije = :NazivKategorije")
    Long getIDKategorije(String NazivKategorije);

    @Query("SELECT  IDIgrac, Igrac.OIB, (Osoba.Ime || ' ' || Osoba.Prezime) AS Ime, Kategorija.NazivKategorije, Igrac.Pozicija, Igrac.Visina, Igrac.Tezina FROM Igrac JOIN Osoba ON Igrac.OIB = Osoba.OIB JOIN Kategorija ON Igrac.IDKategorija = Kategorija.IDKategorija WHERE Igrac.IDKategorija = 1")
    List<PlayerListClass> getAllBasketballSchool();

    @Query("SELECT  IDIgrac, Igrac.OIB, (Osoba.Ime || ' ' || Osoba.Prezime) AS Ime, Kategorija.NazivKategorije, Igrac.Pozicija, Igrac.Visina, Igrac.Tezina FROM Igrac JOIN Osoba ON Igrac.OIB = Osoba.OIB JOIN Kategorija ON Igrac.IDKategorija = Kategorija.IDKategorija WHERE Igrac.IDKategorija = 2")
    List<PlayerListClass> getAllCadets();

    @Query("SELECT  IDIgrac, Igrac.OIB, (Osoba.Ime || ' ' || Osoba.Prezime) AS Ime, Kategorija.NazivKategorije, Igrac.Pozicija, Igrac.Visina, Igrac.Tezina FROM Igrac JOIN Osoba ON Igrac.OIB = Osoba.OIB JOIN Kategorija ON Igrac.IDKategorija = Kategorija.IDKategorija WHERE Igrac.IDKategorija = 3")
    List<PlayerListClass> getAllJuniors();

    @Query("SELECT  IDIgrac, Igrac.OIB, (Osoba.Ime || ' ' || Osoba.Prezime) AS Ime, Kategorija.NazivKategorije, Igrac.Pozicija, Igrac.Visina, Igrac.Tezina FROM Igrac JOIN Osoba ON Igrac.OIB = Osoba.OIB JOIN Kategorija ON Igrac.IDKategorija = Kategorija.IDKategorija WHERE Igrac.IDKategorija = 4")
    List<PlayerListClass> getAllSeniors();
}
