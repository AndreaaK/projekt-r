package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class Kategorija {
    @PrimaryKey(autoGenerate = true)
    public Long IDKategorija;

    @NonNull
    @Size(max = 50)
    public String NazivKategorije;

    public Kategorija() {

    }

    public Kategorija(@NonNull String nazivKategorije) {
        NazivKategorije = nazivKategorije;
    }
}
