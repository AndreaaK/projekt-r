package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.dao.IgracDao;
import hr.fer.wpu.b_ballteam.database.dao.OsobaDao;
import hr.fer.wpu.b_ballteam.database.dao.TajnikDao;
import hr.fer.wpu.b_ballteam.database.dao.TrenerDao;
import hr.fer.wpu.b_ballteam.database.dao.UserDao;
import hr.fer.wpu.b_ballteam.database.dao.ZdrastveniDjelatnikDao;
import hr.fer.wpu.b_ballteam.database.models.Igrac;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Tajnik;
import hr.fer.wpu.b_ballteam.database.models.Trener;
import hr.fer.wpu.b_ballteam.database.models.User;
import hr.fer.wpu.b_ballteam.database.models.ZdrastveniDjelatnik;

public class CreateAccountActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    UserDao db;
    OsobaDao osoba;
    TrenerDao trener;
    TajnikDao tajnik;
    ZdrastveniDjelatnikDao zd;
    IgracDao igrac;
    AppDatabase dataBase;

    EditText newOIB;
    EditText newName;
    EditText newSurname;
    EditText newUsername;
    EditText newPassword;
    Spinner spinner;
    Button btnKreiraj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        newUsername = (EditText) findViewById(R.id.editKreirajUsername);
        newPassword = (EditText) findViewById(R.id.editKreirajPassword);
        newOIB = (EditText) findViewById(R.id.editOIB);
        newName = (EditText) findViewById(R.id.editIme);
        newSurname = (EditText) findViewById(R.id.editPrezime);
        spinner = (Spinner) findViewById(R.id.spinner);
        btnKreiraj = (Button) findViewById(R.id.btnKreiraj);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.roles, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        btnKreiraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String role = spinner.getSelectedItem().toString();
                String username = newUsername.getText().toString();
                String password = newPassword.getText().toString();
                String oib = newOIB.getText().toString();
                String ime = newName.getText().toString();
                String prezime = newSurname.getText().toString();

                if (username.length() > 0 && password.length() > 0 && role.length() > 0
                    && oib.length() == 11 && ime.length() > 0 && prezime.length() > 0) {

                    try{
                        Osoba person = dataBase.getOsobaDao().getOsobaByUsername(username);
                        String ki = person.getKorisničkoIme();
                        Toast.makeText(CreateAccountActivity.this,
                                "Korisničko ime zauzeto", Toast.LENGTH_SHORT).show();
                        return;
                    }catch (NullPointerException e){ }

                    try{
                        Osoba person = dataBase.getOsobaDao().getOsobaByOIB(oib);
                        String o = person.getOIB();
                        Toast.makeText(CreateAccountActivity.this,
                                "OIB zauzet", Toast.LENGTH_SHORT).show();
                        return;
                    }catch (NullPointerException e){}

                    if(role.toLowerCase().equals("igrac")){
                        Intent intent1 = new Intent(CreateAccountActivity.this, CreateIgracActivity.class);
                        intent1.putExtra("uloga", role);
                        intent1.putExtra("KI", username);
                        intent1.putExtra("sifra", password);
                        intent1.putExtra("id", oib);
                        intent1.putExtra("ime", ime);
                        intent1.putExtra("prezime", prezime);
                        startActivity(intent1);
                    }
                    if(role.toLowerCase().equals("trener")){
                        Intent intent1 = new Intent(CreateAccountActivity.this, CreateOstaleUlogeActivity.class);
                        intent1.putExtra("uloga", role);
                        intent1.putExtra("KI", username);
                        intent1.putExtra("sifra", password);
                        intent1.putExtra("id", oib);
                        intent1.putExtra("ime", ime);
                        intent1.putExtra("prezime", prezime);
                        startActivity(intent1);
                    }
                    if(role.toLowerCase().equals("tajnik")){
                        Intent intent1 = new Intent(CreateAccountActivity.this, CreateOstaleUlogeActivity.class);
                        intent1.putExtra("uloga", role);
                        intent1.putExtra("KI", username);
                        intent1.putExtra("sifra", password);
                        intent1.putExtra("id", oib);
                        intent1.putExtra("ime", ime);
                        intent1.putExtra("prezime", prezime);
                        startActivity(intent1);
                    }
                    if(role.toLowerCase().equals("zdravstveni djelatnik")){
                        Intent intent1 = new Intent(CreateAccountActivity.this, CreateOstaleUlogeActivity.class);
                        intent1.putExtra("uloga", role);
                        intent1.putExtra("KI", username);
                        intent1.putExtra("sifra", password);
                        intent1.putExtra("id", oib);
                        intent1.putExtra("ime", ime);
                        intent1.putExtra("prezime", prezime);
                        startActivity(intent1);
                    }

                } else {
                    Toast.makeText(CreateAccountActivity.this,
                            "Pogreška", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}