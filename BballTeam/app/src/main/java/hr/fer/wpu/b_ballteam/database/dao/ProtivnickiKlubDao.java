package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import hr.fer.wpu.b_ballteam.database.models.ProtivnickiKlub;

@Dao
public interface ProtivnickiKlubDao {
    @Query("SELECT * FROM ProtivnickiKlub WHERE NazivKluba = :NazivKluba")
    ProtivnickiKlub getProtivnickiKlub(String NazivKluba);

    @Insert
    void insertProtivnickiKlub(ProtivnickiKlub protivnickiKlub);
}
