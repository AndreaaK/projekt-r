package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.Trener;

@Dao
public interface TrenerDao {
    @Insert
    void insertTrener(Trener trener);
}
