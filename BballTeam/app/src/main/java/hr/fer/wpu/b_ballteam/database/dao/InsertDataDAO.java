package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Query;

import hr.fer.wpu.b_ballteam.database.models.Igrac;

@Dao
public interface InsertDataDAO {
    @Query("INSERT INTO Osoba (OIB, Ime, Prezime, KorisničkoIme, Lozinka, Uloga) VALUES\n" +
            "\t('12345678900', 'Ivan', 'Horvat', 'user1', 'user1', 'igrac'),\n" +
            "\t('12345678901', 'Marko', 'Horvat', 'user2', 'user2', 'igrac'),\n" +
            "\t('12345678902', 'Ivan', 'Ivić', 'user3', 'user3', 'igrac'),\n" +
            "\t('12345678903', 'Marko', 'Markić', 'user4', 'user4', 'igrac'),\n" +
            "\t('12345678904', 'Pero', 'Perić', 'user5', 'user5', 'igrac'),\n" +
            "\t('12345678905', 'Marko', 'Perić', 'user6', 'user6', 'igrac'),\n" +
            "\t('12345678906', 'Ivan', 'Perić', 'user7', 'user7', 'igrac'),\n" +
            "\t('12345678907', 'Mirko', 'Svirko', 'user8', 'user8', 'igrac'), \t\n" +
            "\t('12345678908', 'Mislav', 'Horvat', 'user9', 'user9', 'igrac'),\n" +
            "\t('12345678909', 'Osoba', 'Osobić', 'osoba', 'osoba', 'osoba'),\t\n" +
            "\t('12345678910', 'Admin', 'Adminko', 'admin', 'admin', 'admin'),\n" +
            "\t('12345678911', 'Tajnik', 'Tajnik', 'tajnik', 'tajnik', 'tajnik'),\n" +
            "\t('12345678912', 'Trener', 'Trenerčić', 'trener', 'trener', 'trener'),\n" +
            "\t('12345678913', 'Zdrastveni', 'Djelatnik', 'zd', 'zd', 'zdravstveni'),\n" +
            "\t('98765432100', 'Košarkaški', 'Klub', 'klub', 'klub', 'admin');")
    void insertOsoba();

    @Query("INSERT INTO Trener(OIB) VALUES\n" +
            "\t('12345678912');")
    void insertTrener();

    @Query("INSERT INTO ZdrastveniDjelatnik(OIB) VALUES\n" +
            "\t('12345678909');")
    void insertZdrastveniDjelatnik();

    @Query("INSERT INTO Tajnik(OIB) VALUES\n" +
            "\t('12345678911');")
    void insertTajnik();

    @Query("INSERT INTO Kategorija (NazivKategorije) VALUES \n" +
            "\t('Škola košarke'),\n" +
            "\t('Kadet'),\n" +
            "\t('Junior'),\n" +
            "\t('Senior');")
    void insertKategorija();

    @Query("INSERT INTO Igrac (OIB, DatumRodenja, Pozicija, Visina, Tezina, Broj, IDKategorija ) VALUES\n" +
            "\t('12345678900', '12.03.1997.' ,'PG', 188, 79, 10, 4),\n" +
            "\t('12345678901', '05.07.1989.' ,'SG', 193, 93, 5, 4),\n" +
            "\t('12345678902', '09.04.1997.' ,'C', 201, 100, 7, 4),\n" +
            "\t('12345678903', '20.11.1995.' ,'SF', 191, 88, 9, 4),\n" +
            "\t('12345678904', '17.01.1996.' ,'PF', 192, 88, 20, 4),\n" +
            "\t('12345678905', '11.05.1989.' ,'PG', 183, 77, 17, 4),\n" +
            "\t('12345678906', '24.12.1999.' ,'SF', 185, 80, 3, 3),\n" +
            "\t('12345678907', '15.09.1990.' ,'PG', 183, 80, 22, 1), \t\n" +
            "\t('12345678908', '01.10.1994.' , 'SG', 190, 93, 3, 2);")
    void insertIgrac();

    @Query("INSERT INTO ProtivnickiKlub (NazivKluba) VALUES\n" +
            "\t('Tim Tim'),\n" +
            "\t('Novi Tim'),\n" +
            "\t('PrvakTim'),\n" +
            "\t('Tim ABC'),\n" +
            "\t('NekiDrugiTim');")
    void insertProtivnickiKlub();

    @Query("INSERT INTO Utakmica(Pocetak, Kraj, RezultatUtakmice, IDProtivnik) VALUES\n" +
            "\t('21.10.2020. 10:30:00', '21.10.2020. 11:45:00', '77:68', 5),\n" +
            "\t('29.10.2020. 10:30:00', '29.10.2020. 11:45:00', '81:75', 1),\n" +
            "\t('4.11.2020. 15:00:00', '4.11.2020. 16:15:00', '80:81', 1),\n" +
            "\t('9.11.2020. 12:45:00', '9.11.2020. 14:00:00', '89:62', 2),\n" +
            "\t('17.11.2020. 10:30:00', '17.11.2020. 11:45:00', '59:55', 4),\n" +
            "\t('24.11.2020. 08:30:00', '24.11.2020. 09:45:00', '77:83', 3),\n" +
            "\t('30.11.2020. 10:00:00', '30.11.2020. 11:15:00', '82:76', 4),\n" +
            "\t('31.11.2020. 17:00:00', '31.11.2020. 18:45:00', '77:77', 2),\n" +
            "\t('1.12.2020. 11:00:00', '', '', 1);")
    void insertUtakmica();

    @Query("INSERT INTO VrstaTransakcije(IDVrstaTransakcija, naziv) VALUES\n" +
            "\t (1, 'Stavi na račun'),\n" +
            "\t (2, 'Skini s računa');")
    void insertVrstaTransakcije();

    @Query("INSERT INTO Transakcija(iznos, datumTransakcije, OIB, IDVrstaTransakcija) VALUES\n" +
            "\t(10000.00, '12.11.2020. 10:00:00', '12345678900', 1),\n" +
            "\t(10000.00, '12.11.2020. 12:00:00', '12345678900', 2),\n" +
            "\t(10000.00, '12.12.2020. 10:00:00', '12345678900', 1),\n" +
            "\t(10000.00, '12.12.2020. 12:00:00', '12345678900', 2),\n" +
            "\t(10000.00, '12.01.2021. 10:00:00', '12345678900', 1),\n" +
            "\t(1000000.00, '01.10.2020. 10:00:00', '98765432100', 1);")
    void insertTransakcije();

    @Query("INSERT INTO PlanAktivnosti(IDPLanAktivnosti, Sadrzaj, Dan, VrijemePočetka, Detalj, Lokacija, IDTrener, IDZdrastveniDjelatnik, IDKategorija) VALUES\n" +
            "\t(1, 'Trening', 0, '09:00', 'Nakon treninga ćemo održati kratki sastanak, max. 30 min', 'Dom sportova', 1, 1, 4),\n" +
            "\t(2, 'Trening', 1, '09:00', 'Donjeti bijele majce', 'Dom sportova', 1, 1, 4),\n" +
            "\t(3, 'Trening', 2, '16:00', 'Igrači Ivić i Horvat poslije treninga imaju pregled kod Dr. Joze', 'Dom sportova', 1, 1, 4),\n" +
            "\t(4, 'Trening', 3, '09:00', 'Nema detalja', 'Dom sportova', 1, 1, 4),\n" +
            "\t(5, 'Trening', 4, '09:30', 'Sastanak u 16h, dogovaranje detalja oko taktike za sutrašnju utkamicu.', 'Dom sportova', 1, 1, 4),\n" +
            "\t(6, 'Utakmica', 5, '10:00', 'Igramo gostujuću utakmicu, ponijeti tamno-plave dresove.', 'Draženov dom', 1, 1, 4),\n" +
            "\t(7, 'Odmor', 6, '', 'Dan odmora', 'Doma', 1, 1, 4);")
    void insertPlanAktivnosti();
}
