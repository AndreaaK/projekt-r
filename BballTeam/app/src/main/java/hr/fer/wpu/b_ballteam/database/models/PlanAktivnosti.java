package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"IDPLanAktivnosti", "Dan"},
        foreignKeys = {@ForeignKey(entity = Trener.class, parentColumns = "IDTrener", childColumns = "IDTrener", onDelete = CASCADE),
                @ForeignKey(entity = Kategorija.class, parentColumns = "IDKategorija", childColumns = "IDKategorija", onDelete = CASCADE),
                @ForeignKey(entity = ZdrastveniDjelatnik.class, parentColumns = "IDZdrastveniDjelatnik", childColumns = "IDZdrastveniDjelatnik", onDelete = CASCADE)},
        indices = {@Index(value = {"IDTrener"}), @Index(value = "IDZdrastveniDjelatnik"), @Index(value = "IDKategorija")})
@Data
public class PlanAktivnosti {
    @NonNull
    public int IDPLanAktivnosti;

    @NonNull
    @Size(max = 150)
    public String Sadrzaj;

    @NonNull
    public int Dan;

    @NonNull
    public String VrijemePočetka;

    @NonNull
    public String Detalj;

    @NonNull
    public String Lokacija;

    @NonNull
    public long IDTrener;

    @NonNull
    public int IDZdrastveniDjelatnik;

    @NonNull
    public int IDKategorija;

    public PlanAktivnosti() {

    }

    public PlanAktivnosti(@NonNull String sadrzaj, int dan, long IDTrener, int IDZdrastveniDjelatnik, int IDKategorija) {
        Sadrzaj = sadrzaj;
        Dan = dan;
        this.IDTrener = IDTrener;
        this.IDZdrastveniDjelatnik = IDZdrastveniDjelatnik;
        this.IDKategorija = IDKategorija;
    }
}
