package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import hr.fer.wpu.b_ballteam.database.models.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM User WHERE userName = :userName AND password = :password")
    User login(String userName, String password);

    @Insert
    void insert(User user);
}
