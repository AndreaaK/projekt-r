package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.TypeConverters;

import java.util.Date;

import hr.fer.wpu.b_ballteam.converter.DateConverter;
import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"DatumPocetkaOzljede", "IDIgrac", "IDOzljeda"},
        foreignKeys = {@ForeignKey(entity = Igrac.class, parentColumns = "IDIgrac", childColumns = "IDIgrac", onDelete = CASCADE),
                @ForeignKey(entity = Ozljeda.class, parentColumns = "IDOzljeda", childColumns = "IDOzljeda", onDelete = CASCADE)},
        indices = {@Index(value = "IDIgrac"), @Index(value = "IDOzljeda")})
@Data
public class ImaOzljedu {
    @NonNull
    public Long IDIgrac;

    @NonNull
    public Long IDOzljeda;

    @NonNull
    @TypeConverters(DateConverter.class)
    public Date DatumPocetkaOzljede;

    @NonNull
    @TypeConverters(DateConverter.class)
    public Date DatumKrajaOzljede;

    public ImaOzljedu() {

    }

    public ImaOzljedu(@NonNull Long IDIgrac, @NonNull Long IDOzljeda, @NonNull Date datumPocetkaOzljede, @NonNull Date datumKrajaOzljede) {
        this.IDIgrac = IDIgrac;
        this.IDOzljeda = IDOzljeda;
        DatumPocetkaOzljede = datumPocetkaOzljede;
        DatumKrajaOzljede = datumKrajaOzljede;
    }
}
