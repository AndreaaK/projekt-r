package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ClubInfoActivity extends AppCompatActivity {

    TextView lokacija;
    TextView telefon;
    TextView email;
    TextView telefon2;
    TextView email2;
    TextView telefon3;
    TextView email3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_info);

        lokacija = (TextView) findViewById(R.id.adresa);
        lokacija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:45.801942,15.973078"));
                startActivity(intent);
            }
        });

        telefon = (TextView) findViewById(R.id.telefon);
        telefon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01/1234-56"));
                startActivity(intent);
            }
        });

        email = (TextView) findViewById(R.id.email);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL,
                        "b@ballteam.hr");
                intent.putExtra(Intent.EXTRA_SUBJECT,
                        "[Bball info]");
                startActivity(intent);
            }
        });
        telefon2 = (TextView) findViewById(R.id.telefon2);
        telefon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01/1234-78"));
                startActivity(intent);
            }
        });

        email2 = (TextView) findViewById(R.id.email2);
        email2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL,
                        "marketing@ballteam.hr");
                intent.putExtra(Intent.EXTRA_SUBJECT,
                        "[Bball marketing]");
                startActivity(intent);
            }
        });

        telefon3 = (TextView) findViewById(R.id.telefon3);
        telefon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01/1234-78"));
                startActivity(intent);
            }
        });

        email3 = (TextView) findViewById(R.id.email3);
        email3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL,
                        "t.osoblje@ballteam.hr");
                intent.putExtra(Intent.EXTRA_SUBJECT,
                        "[Bball tehnicko osoblje]");
                startActivity(intent);
            }
        });


    }
}