package hr.fer.wpu.b_ballteam;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.PlayerListClass;

public class KadetiCategoryActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seniori);

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<PlayerListClass> igraci = database.getKategorijaDao().getAllCadets();

        String mTitle[] = new String[igraci.size()];
        String mDescription[] = new String[igraci.size()];
        int images[] = new int[igraci.size()];
        int ids[] = new int[igraci.size()];
        String[] oib = new String[igraci.size()];

        for (int i = 0; i < igraci.size(); i++) {
            mTitle[i] = igraci.get(i).getIme();
            mDescription[i] = igraci.get(i).getPozicija() + ", " + igraci.get(i).getVisina() + " cm , " + igraci.get(i).getTezina() + " kg";
            images[i] = R.drawable.player_icon;
            ids[i] = igraci.get(i).getIDIgrac();
            oib[i] = igraci.get(i).getOIB();

        }

        listView = findViewById(R.id.playerList);

        KadetiCategoryActivity.MyAdapter adapter = new KadetiCategoryActivity.MyAdapter(this, mTitle, mDescription, images, ids, oib);
        listView.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];
        int ids[];
        String oib[];

        MyAdapter(Context c, String title[], String description[], int imgs[], int ids[], String oib[]) {
            super(c, R.layout.row, R.id.imePrezime, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;
            this.rImgs = imgs;
            this.ids = ids;
            this.oib = oib;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.imePrezime);
            TextView myDescription = row.findViewById(R.id.opis);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(KadetiCategoryActivity.this, PlayerProfileActivity.class);
                    Bundle extras = new Bundle();
                    extras.putInt("id", ids[position]);
                    extras.putString("oib", oib[position]);
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });

            images.setImageResource(rImgs[position]);
            myTitle.setText(rTitle[position]);
            myDescription.setText(rDescription[position]);

            return row;
        }
    }
}
