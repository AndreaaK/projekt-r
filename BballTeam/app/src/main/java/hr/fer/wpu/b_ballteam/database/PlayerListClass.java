package hr.fer.wpu.b_ballteam.database;

import lombok.Data;

@Data
public class PlayerListClass {
    private String Ime;
    private String NazivKategorije;
    private String Pozicija;
    private String Visina;
    private String Tezina;
    private int IDIgrac;
    private String OIB;
}
