package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.TypeConverters;

import java.util.Date;

import hr.fer.wpu.b_ballteam.converter.DateConverter;
import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"DatumPocetkaSuspenzije", "IDIgrac", "IDVrstaSuspenzije"},
        foreignKeys = {@ForeignKey(entity = Igrac.class, parentColumns = "IDIgrac", childColumns = "IDIgrac", onDelete = CASCADE),
                @ForeignKey(entity = Suspendiran.class, parentColumns = "IDVrstaSuspenzije", childColumns = "IDVrstaSuspenzije", onDelete = CASCADE)},
        indices = {@Index(value = "IDIgrac"), @Index(value = "IDVrstaSuspenzije")})
@Data
public class MozeBiti {
    @NonNull
    public Long IDIgrac;

    @NonNull
    public Long IDVrstaSuspenzije;

    @NonNull
    @TypeConverters(DateConverter.class)
    public Date DatumPocetkaSuspenzije;

    @NonNull
    @TypeConverters(DateConverter.class)
    public Date DatumKrajaSuspenzije;

    public MozeBiti() {

    }

    public MozeBiti(@NonNull Long IDIgrac, @NonNull Long IDVrstaSuspenzije, @NonNull Date datumPocetkaSuspenzije, @NonNull Date datumKrajaSuspenzije) {
        this.IDIgrac = IDIgrac;
        this.IDVrstaSuspenzije = IDVrstaSuspenzije;
        DatumPocetkaSuspenzije = datumPocetkaSuspenzije;
        DatumKrajaSuspenzije = datumKrajaSuspenzije;
    }
}
