package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import hr.fer.wpu.b_ballteam.database.AppDatabase;

public class CheckMembersFinanceActivity extends AppCompatActivity {

    Button btnPregledaj;
    AppDatabase dataBase;
    AutoCompleteTextView enterName;
    String[] osobe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_members_finance);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        enterName = findViewById(R.id.editOIB3);
        osobe = dataBase.getOsobaDao().getAllOsoba();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, osobe);

        enterName.setAdapter(adapter);

        btnPregledaj = (Button) findViewById(R.id.btnPregledTransakcija);

        btnPregledaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedText = enterName.getText().toString();
                String[] oib = selectedText.split(", OIB: ");

                try{
                    if(oib[1].length() != 11){
                        Toast.makeText(CheckMembersFinanceActivity.this, "Neispravan OIB", Toast.LENGTH_SHORT).show();
                    }
                    else if(!dataBase.getOsobaDao().CheckOsoba(oib[1]).equals(oib[0])){
                        Toast.makeText(CheckMembersFinanceActivity.this, "Upisani OIB ne pripada upisanoj osobi", Toast.LENGTH_SHORT).show();
                    }
                    else if(dataBase.getTransakcijaDao().getAllTransactionsFrom(oib[1]).size() == 0){
                        Toast.makeText(CheckMembersFinanceActivity.this, "Osoba trenutno nema transakcija", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Intent intent = new Intent(CheckMembersFinanceActivity.this, MemberTransactionsActivity.class);
                        intent.putExtra("memberOIB", oib[1]);
                        startActivity(intent);
                    }
                }catch (Exception ex){
                    Toast.makeText(CheckMembersFinanceActivity.this, "Neispravan unos podataka", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}