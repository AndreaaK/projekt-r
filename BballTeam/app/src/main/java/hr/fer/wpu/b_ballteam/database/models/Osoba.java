package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity(indices = {@Index(value = {"KorisničkoIme"}, unique = true)})
@Data
public class Osoba {
    @PrimaryKey
    @NonNull
    @Size(min = 11, max = 11)
    public String OIB;

    @NonNull
    @Size(max = 30)
    public String Ime;

    @NonNull
    @Size(max = 30)
    public String Prezime;

    @NonNull
    @Size(max = 30)
    public String KorisničkoIme;

    @NonNull
    @Size(max = 20)
    public String Lozinka;

    @NonNull
    @Size(max = 20)
    public String Uloga;

    public Osoba() {

    }

    public Osoba(@NonNull String OIB, @NonNull String ime, @NonNull String prezime, @NonNull String korisničkoIme, @NonNull String lozinka, @NonNull String uloga) {
        this.OIB = OIB;
        Ime = ime;
        Prezime = prezime;
        KorisničkoIme = korisničkoIme;
        Lozinka = lozinka;
        Uloga = uloga;
    }

}
