package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import hr.fer.wpu.b_ballteam.database.models.Osoba;

@Dao
public interface OsobaDao {
    @Query("SELECT * FROM Osoba WHERE korisničkoIme = :korisničkoIme")
    Osoba getOsobaByUsername(String korisničkoIme);

    @Query("SELECT * FROM Osoba WHERE OIB = :OIB")
    Osoba getOsobaByOIB(String OIB);

    @Insert
    void insertOsoba(Osoba osoba);

    @Query("SELECT * FROM Osoba WHERE KorisničkoIme = :userName AND Lozinka = :password")
    Osoba login(String userName, String password);

    @Query("SELECT ( Ime || ' ' || Prezime || ', OIB: ' || OIB ) AS imePrezimeOIB FROM Osoba")
    String[] getAllOsoba();

    @Query("SELECT ( Ime || ' ' || Prezime ) AS imePrezimeOIB FROM Osoba WHERE OIB = :oib")
    String CheckOsoba(String oib);

    @Query("SELECT Uloga FROM Osoba WHERE KorisničkoIme = :Username")
    String getUloga(String Username);
}
