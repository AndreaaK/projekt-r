package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class CheckFinanceActivity extends AppCompatActivity {

    Button btnFinancijeKluba;
    Button btnFinancijeClanova;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_finance);

        btnFinancijeKluba = (Button) findViewById(R.id.btnFinancijeKluba);
        btnFinancijeClanova = (Button) findViewById(R.id.btnFinancijeClanova);

        btnFinancijeKluba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckFinanceActivity.this, CheckClubFinance.class);
                startActivity(intent);
            }
        });
        btnFinancijeClanova.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(CheckFinanceActivity.this, CheckMembersFinanceActivity.class);
                startActivity(intent2);
            }
        });

    }
}