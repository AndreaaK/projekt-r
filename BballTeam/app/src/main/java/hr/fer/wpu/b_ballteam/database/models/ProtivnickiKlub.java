package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class ProtivnickiKlub {
    @PrimaryKey(autoGenerate = true)
    public Long IDProtivnik;

    @NonNull
    @Size(max = 70)
    public String NazivKluba;

    public ProtivnickiKlub() {

    }

    public ProtivnickiKlub(@NonNull String nazivKluba) {
        NazivKluba = nazivKluba;
    }
}
