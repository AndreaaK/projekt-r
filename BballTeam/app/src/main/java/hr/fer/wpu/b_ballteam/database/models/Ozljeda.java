package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class Ozljeda {
    @PrimaryKey(autoGenerate = true)
    public Long IDOzljeda;

    @NonNull
    @Size(max = 50)
    public String VrstaOzljede;

    @Size(max = 150)
    public String OpisOzljede;

    public Ozljeda() {

    }

    public Ozljeda(@NonNull String vrstaOzljede, String opisOzljede) {
        VrstaOzljede = vrstaOzljede;
        OpisOzljede = opisOzljede;
    }
}
