package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.ImaOzljedu;

@Dao
public interface ImaOzljeduDao {
    @Insert
    void insertImaOzljedu(ImaOzljedu imaOzljedu);
}
