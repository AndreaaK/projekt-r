package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.GamesClass;
import hr.fer.wpu.b_ballteam.database.models.Utakmica;

@Dao
public interface UtakmicaDao {
    @Insert
    void insertUtakmica(Utakmica utakmica);

    @Query("SELECT Pocetak, Kraj, RezultatUtakmice, NazivKluba FROM Utakmica JOIN ProtivnickiKlub ON Utakmica.IDProtivnik = ProtivnickiKlub.IDProtivnik WHERE Kraj IS NOT ''")
    List<GamesClass> getPastGames();

    @Query("SELECT Pocetak, Kraj, RezultatUtakmice, NazivKluba FROM Utakmica JOIN ProtivnickiKlub ON Utakmica.IDProtivnik = ProtivnickiKlub.IDProtivnik WHERE Kraj IS ''")
    List<GamesClass> getFutureGames();
}
