package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class DnevnikAktivnostiActivity extends AppCompatActivity {

    LinearLayout seniori;
    LinearLayout juniori;
    LinearLayout kadeti;
    LinearLayout skolakosarke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dnevnik_aktivnosti);

        //postavi senior button
        seniori = (LinearLayout)findViewById(R.id.senioriactivity);
        seniori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DnevnikAktivnostiActivity.this, RasporedAktivnostiActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("id", 4);
                extras.putString("Username", getIntent().getStringExtra("Username"));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        juniori = (LinearLayout)findViewById(R.id.junioriactivity);
        juniori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DnevnikAktivnostiActivity.this, RasporedAktivnostiActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("id", 3);
                extras.putString("Username", getIntent().getStringExtra("Username"));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        kadeti = (LinearLayout)findViewById(R.id.kadetiactivity);
        kadeti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DnevnikAktivnostiActivity.this, RasporedAktivnostiActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("id", 2);
                extras.putString("Username", getIntent().getStringExtra("Username"));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        skolakosarke = (LinearLayout)findViewById(R.id.skolaactivity);
        skolakosarke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DnevnikAktivnostiActivity.this, RasporedAktivnostiActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("id", 1);
                extras.putString("Username", getIntent().getStringExtra("Username"));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });




    }
}