package hr.fer.wpu.b_ballteam.database;

import androidx.room.TypeConverters;

import java.util.Date;

import hr.fer.wpu.b_ballteam.converter.DateConverter;
import lombok.Data;

@Data
public class GamesClass {
    @TypeConverters(DateConverter.class)
    private String Pocetak;

    @TypeConverters(DateConverter.class)
    private String Kraj;

    private String RezultatUtakmice;

    private String NazivKluba;
}
