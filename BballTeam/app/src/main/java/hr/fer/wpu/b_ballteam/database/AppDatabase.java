package hr.fer.wpu.b_ballteam.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.io.File;

import hr.fer.wpu.b_ballteam.database.dao.IgracDao;
import hr.fer.wpu.b_ballteam.database.dao.ImaOzljeduDao;
import hr.fer.wpu.b_ballteam.database.dao.InsertDataDAO;
import hr.fer.wpu.b_ballteam.database.dao.KategorijaDao;
import hr.fer.wpu.b_ballteam.database.dao.MozeBitiDao;
import hr.fer.wpu.b_ballteam.database.dao.OsobaDao;
import hr.fer.wpu.b_ballteam.database.dao.OzljedaDao;
import hr.fer.wpu.b_ballteam.database.dao.PlanAktivnostiDao;
import hr.fer.wpu.b_ballteam.database.dao.ProtivnickiKlubDao;
import hr.fer.wpu.b_ballteam.database.dao.SudjelujeNaDao;
import hr.fer.wpu.b_ballteam.database.dao.SuspendiranDao;
import hr.fer.wpu.b_ballteam.database.dao.TajnikDao;
import hr.fer.wpu.b_ballteam.database.dao.TransakcijaDao;
import hr.fer.wpu.b_ballteam.database.dao.TrenerDao;
import hr.fer.wpu.b_ballteam.database.dao.UserDao;
import hr.fer.wpu.b_ballteam.database.dao.UtakmicaDao;
import hr.fer.wpu.b_ballteam.database.dao.VrstaTransakcijeDao;
import hr.fer.wpu.b_ballteam.database.dao.ZdrastveniDjelatnikDao;
import hr.fer.wpu.b_ballteam.database.models.Igrac;
import hr.fer.wpu.b_ballteam.database.models.ImaOzljedu;
import hr.fer.wpu.b_ballteam.database.models.Kategorija;
import hr.fer.wpu.b_ballteam.database.models.MozeBiti;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Ozljeda;
import hr.fer.wpu.b_ballteam.database.models.PlanAktivnosti;
import hr.fer.wpu.b_ballteam.database.models.ProtivnickiKlub;
import hr.fer.wpu.b_ballteam.database.models.SudjelujeNa;
import hr.fer.wpu.b_ballteam.database.models.Suspendiran;
import hr.fer.wpu.b_ballteam.database.models.Tajnik;
import hr.fer.wpu.b_ballteam.database.models.Transakcija;
import hr.fer.wpu.b_ballteam.database.models.Trener;
import hr.fer.wpu.b_ballteam.database.models.User;
import hr.fer.wpu.b_ballteam.database.models.Utakmica;
import hr.fer.wpu.b_ballteam.database.models.VrstaTransakcije;
import hr.fer.wpu.b_ballteam.database.models.ZdrastveniDjelatnik;

@Database(entities = {Osoba.class, User.class, Trener.class, ZdrastveniDjelatnik.class, Tajnik.class, Kategorija.class, Ozljeda.class, ProtivnickiKlub.class,
        Suspendiran.class, Igrac.class, Utakmica.class, PlanAktivnosti.class, Transakcija.class, VrstaTransakcije.class, SudjelujeNa.class, ImaOzljedu.class,
        MozeBiti.class}, version = 27, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "Basketball.db")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDao getUserDao();

    public abstract OsobaDao getOsobaDao();

    public abstract TrenerDao getTrenerDao();

    public abstract ZdrastveniDjelatnikDao getZdrastveniDjelatnikDao();

    public abstract TajnikDao getTajnikDao();

    public abstract KategorijaDao getKategorijaDao();

    public abstract OzljedaDao getOzljedaDao();

    public abstract ProtivnickiKlubDao getProtivnickiKlubDao();

    public abstract SuspendiranDao getSuspendiranDao();

    public abstract VrstaTransakcijeDao getVrstaTransakcijeDao();

    public abstract IgracDao getIgracDao();

    public abstract UtakmicaDao getUtakmicaDao();

    public abstract PlanAktivnostiDao getPlanAktivnostiDao();

    public abstract TransakcijaDao getTransakcijaDao();

    public abstract SudjelujeNaDao getSudjelujeNaDao();

    public abstract ImaOzljeduDao getImaOzljeduDao();

    public abstract MozeBitiDao getMozeBitiDao();

    public abstract InsertDataDAO getInsertDataDao();
}
