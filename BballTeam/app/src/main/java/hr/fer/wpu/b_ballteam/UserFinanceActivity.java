package hr.fer.wpu.b_ballteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Transakcija;

public class UserFinanceActivity extends AppCompatActivity {

    Button btnTransakcije;
    TextView stanjeRacuna;
    AppDatabase dataBase;
    String OIB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_finance);

        btnTransakcije = (Button) findViewById(R.id.btnTransakcije);
        stanjeRacuna = (TextView) findViewById(R.id.textViewIznos);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        String userName = getIntent().getStringExtra("Username");
        Osoba osoba = dataBase.getOsobaDao().getOsobaByUsername(userName);
        OIB = osoba.getOIB();
        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<Transakcija> Transakcije = database.getTransakcijaDao().getAllTransactionsFrom(OIB);

        Double stanje = (double)0;

        for (int i = 0; i < Transakcije.size(); i++) {
            Long vrsta = Transakcije.get(i).getIDVrstaTransakcija();
            Double av = Math.abs(Transakcije.get(i).getIznos());
            if (vrsta == (long)1){av = +av;}
            else{av = -av;}
            stanje += av;
        }

        stanjeRacuna.setText(String.format("%.2f", stanje) + " kn ");


        btnTransakcije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(database.getTransakcijaDao().getAllTransactionsFrom(OIB).size() == 0){
                    Toast.makeText(UserFinanceActivity.this, "Osoba trenutno nema transakcija", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(UserFinanceActivity.this, CheckTransactionActivity.class);
                    intent.putExtra("Username", getIntent().getStringExtra("Username"));
                    startActivity(intent);
                }
            }
        });
    }
}