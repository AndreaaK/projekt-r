package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class Suspendiran {
    @PrimaryKey(autoGenerate = true)
    public Long IDVrstaSuspenzije;

    @NonNull
    @Size(max = 50)
    public String Razlog;

    public Suspendiran() {

    }

    public Suspendiran(@NonNull String razlog) {
        Razlog = razlog;
    }
}
