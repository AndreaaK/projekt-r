package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.ZdrastveniDjelatnik;

@Dao
public interface ZdrastveniDjelatnikDao {
    @Insert
    void insertZdrastveniDjelatnik(ZdrastveniDjelatnik zdrastveniDjelatnik);
}
