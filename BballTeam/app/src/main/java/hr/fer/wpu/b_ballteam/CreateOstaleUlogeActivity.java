package hr.fer.wpu.b_ballteam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import hr.fer.wpu.b_ballteam.R;
import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Tajnik;
import hr.fer.wpu.b_ballteam.database.models.Trener;
import hr.fer.wpu.b_ballteam.database.models.ZdrastveniDjelatnik;

public class CreateOstaleUlogeActivity extends AppCompatActivity {

    AppDatabase dataBase;

    TextView text1;
    TextView text2;
    TextView text3;

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ostale_uloge);

        text1 = (TextView) findViewById(R.id.textInfo1);
        text2 = (TextView) findViewById(R.id.textUloga);
        text3 = (TextView) findViewById(R.id.textProvjera);

        btn = (Button) findViewById(R.id.btnStvori2);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        String role = getIntent().getStringExtra("uloga");

        text2.setText(role);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = getIntent().getStringExtra("KI");
                String password = getIntent().getStringExtra("sifra");
                String oib = getIntent().getStringExtra("id");
                String ime = getIntent().getStringExtra("ime");
                String prezime = getIntent().getStringExtra("prezime");

                Osoba osoba = new Osoba(oib, ime, prezime, username, password, role);
                dataBase.getOsobaDao().insertOsoba(osoba);

                Log.i("role", role);

                if(role.toLowerCase().equals("admin")){
                    Tajnik admin = new Tajnik(oib);
                    dataBase.getTajnikDao().insertTajnik(admin);
                    Toast.makeText(CreateOstaleUlogeActivity.this,
                            "Unesen admin", Toast.LENGTH_SHORT).show();
                }
                if(role.toLowerCase().equals("trener")){
                    Trener trener = new Trener(oib);
                    dataBase.getTrenerDao().insertTrener(trener);
                    Toast.makeText(CreateOstaleUlogeActivity.this,"Unesen trener", Toast.LENGTH_SHORT).show();
                }
                if(role.toLowerCase().equals("zdravstveni djelatnik")){
                    ZdrastveniDjelatnik zd = new ZdrastveniDjelatnik(oib);
                    dataBase.getZdrastveniDjelatnikDao().insertZdrastveniDjelatnik(zd);
                    Toast.makeText(CreateOstaleUlogeActivity.this,
                            "Unesen zdravstveni djelatnik", Toast.LENGTH_SHORT).show();
                }
                if(role.toLowerCase().equals("tajnik")){
                    Tajnik tajnik = new Tajnik(oib);
                    dataBase.getTajnikDao().insertTajnik(tajnik);
                    Toast.makeText(CreateOstaleUlogeActivity.this,
                            "Unesen tajnik", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}