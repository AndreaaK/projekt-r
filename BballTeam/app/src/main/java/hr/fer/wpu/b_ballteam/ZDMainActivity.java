package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ZDMainActivity extends AppCompatActivity {

    Button btnPZSI;
    Button btnUDA;
    Button btnPocetna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_z_d_main);

        btnPZSI = (Button) findViewById(R.id.btnPZSI);
        btnUDA = (Button) findViewById(R.id.btnUDA);
        btnPocetna = (Button) findViewById(R.id.btnMain);

        btnPZSI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ZDMainActivity.this, PZSIActivity.class);
                startActivity(intent);
            }
        });

        btnUDA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(ZDMainActivity.this, UDAktivnostiActivity.class);
                startActivity(intent2);
            }
        });

        btnPocetna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(ZDMainActivity.this, MainActivity.class);
                intent3.putExtra("Username", getIntent().getStringExtra("Username"));
                startActivity(intent3);
            }
        });
    }
}