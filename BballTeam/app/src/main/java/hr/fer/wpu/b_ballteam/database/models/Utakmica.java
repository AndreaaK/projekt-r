package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import hr.fer.wpu.b_ballteam.converter.DateConverter;
import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = ProtivnickiKlub.class, parentColumns = "IDProtivnik", childColumns = "IDProtivnik", onDelete = CASCADE),
        indices = {@Index(value = {"Pocetak"}, unique = true), @Index(value = {"IDProtivnik"})})
@Data
public class Utakmica {
    @PrimaryKey(autoGenerate = true)
    public Long IDUtakmica;

    @NonNull
    public String Pocetak;

    public String Kraj;

    @Size(max = 10)
    public String RezultatUtakmice;

    @NonNull
    public int IDProtivnik;

    public Utakmica() {

    }

    public Utakmica(@NonNull String pocetak, @NonNull String kraj, @NonNull String rezultatUtakmice, int IDProtivnik) {
        Pocetak = pocetak;
        Kraj = kraj;
        RezultatUtakmice = rezultatUtakmice;
        this.IDProtivnik = IDProtivnik;
    }
}
