package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class CategoryActivity extends AppCompatActivity {
    Button btnSeniori;
    Button btnJuniori;
    Button btnKadeti;
    Button btnSkolaKosarke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        btnSeniori = (Button) findViewById(R.id.seniori);
        btnJuniori = (Button) findViewById(R.id.juniori);
        btnKadeti = (Button) findViewById(R.id.kadeti);
        btnSkolaKosarke = (Button) findViewById(R.id.skolaKosarke);

        btnSeniori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, SeniorCategoryActivity.class);
                startActivity(intent);
            }
        });

        btnJuniori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, JuniorCategory.class);
                startActivity(intent);
            }
        });

        btnKadeti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, KadetiCategoryActivity.class);
                startActivity(intent);
            }
        });

        btnSkolaKosarke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, SkolaKosarkeActivity.class);
                startActivity(intent);
            }
        });

    }

}
