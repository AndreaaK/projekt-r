package hr.fer.wpu.b_ballteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import hr.fer.wpu.b_ballteam.database.AppDatabase;

public class DanDetaljiActivity extends AppCompatActivity {
    int id;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dan_detalji);

        //dohvat iz baze i prezimanje dodataka
        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        Intent intent = new Intent(this, DanDetaljiActivity.class);
        Bundle extras = getIntent().getExtras();
        id =  extras.getInt("id");
        username = extras.getString("Username");

        //provjera ispisa
        Log.i("wooho", id +"");

        //postavi sadrzaj i vrijeme
        String sadrzaj = database.getPlanAktivnostiDao().getVrijemePocetak(id) + " " + database.getPlanAktivnostiDao().getSadrzaj(id);
        TextView sadrzajView = findViewById(R.id.sadrzajOpis);
        sadrzajView.setText(sadrzaj);

        //postavi lokaciju
        String lokacija = database.getPlanAktivnostiDao().getLokacija(id);
        TextView lokacijaView = findViewById(R.id.lokacija);
        lokacijaView.setText(lokacija);

        //na klik otvori google maps prikaz
        lokacijaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:45.801942,15.973078"));
                startActivity(intent);
            }
        });


        //Postavi detalje
        String detalji = database.getPlanAktivnostiDao().getDetalji(id);
        TextView detaljiView = findViewById(R.id.detaljiOpis);
        detaljiView.setText(detalji);

        Button btnPocetna = findViewById(R.id.pocetna);
        btnPocetna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DanDetaljiActivity.this, MainActivity.class);
                Bundle extras = new Bundle();
                extras.putString("Username", getIntent().getExtras().getString("Username"));
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        Button btnUredi = findViewById(R.id.uredi);
        btnUredi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uloga = database.getOsobaDao().getUloga(username);
                if(uloga.toUpperCase().equals("TRENER") || uloga.toUpperCase().contains("ZDRAVSTVENI")) {
                    Intent intent = new Intent(DanDetaljiActivity.this, Coach_activity_plan.class);
                    Bundle extras = new Bundle();
                    extras.putInt("id", id);
                    intent.putExtras(extras);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(DanDetaljiActivity.this, "Niste ovlašteni za uređivanje dnevnika aktivnosti", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}