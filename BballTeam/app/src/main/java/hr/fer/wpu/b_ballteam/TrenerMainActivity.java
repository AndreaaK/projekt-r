package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class TrenerMainActivity extends AppCompatActivity {

    Button btnUPI;
    Button btnUDA;
    Button btnPocetna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trener_main);

        btnUPI = (Button) findViewById(R.id.btnUPI);
        btnUDA = (Button) findViewById(R.id.btnUDA);
        btnPocetna = (Button) findViewById(R.id.btnMain);

        btnUPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrenerMainActivity.this, UPIgracaActivity.class);
                startActivity(intent);
            }
        });

        btnUDA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(TrenerMainActivity.this, UDAktivnostiActivity.class);
                startActivity(intent2);
            }
        });

        btnPocetna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(TrenerMainActivity.this, MainActivity.class);
                intent3.putExtra("Username", getIntent().getStringExtra("Username"));
                startActivity(intent3);
            }
        });
    }
}