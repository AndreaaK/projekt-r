package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Transakcija;

public class CheckClubFinance extends AppCompatActivity {

    Button btnNovaTransakcija;
    TextView stanjeRacuna;
    AppDatabase dataBase;

    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_club_finance);

        btnNovaTransakcija = (Button) findViewById(R.id.btnNovaTransakcija);
        stanjeRacuna = (TextView) findViewById(R.id.textViewIznosKluba);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<Transakcija> Transakcije = database.getTransakcijaDao().getAllTransactionsFrom("98765432100");

        Double stanje = (double)0;

        for (int i = 0; i < Transakcije.size(); i++) {
            Long vrsta = Transakcije.get(i).getIDVrstaTransakcija();
            Double av = Math.abs(Transakcije.get(i).getIznos());
            if (vrsta == (long)1){av = +av;}
            else{av = -av;}
            stanje += av;
        }

        stanjeRacuna.setText(String.format("%.2f", stanje) + " kn ");

        btnNovaTransakcija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckClubFinance.this, NewTransactionActivity.class);
                startActivity(intent);
            }
        });
    }
}