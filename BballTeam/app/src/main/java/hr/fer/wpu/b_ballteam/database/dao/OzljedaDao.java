package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.Ozljeda;

@Dao
public interface OzljedaDao {
    @Insert
    void insertOzljeda(Ozljeda ozljeda);
}
