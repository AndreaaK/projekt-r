package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

@Entity
@Data
public class VrstaTransakcije {
    @PrimaryKey
    @NonNull
    public Long IDVrstaTransakcija;

    @NonNull
    @Size(max = 50)
    public String Naziv;

    public VrstaTransakcije() {

    }

    public VrstaTransakcije(@NonNull Long IDVrstaTransakcija, @NonNull String naziv) {
        this.IDVrstaTransakcija = IDVrstaTransakcija;
        Naziv = naziv;
    }

    public Long getIDVrstaTransakcije() {return this.IDVrstaTransakcija;}

    public String getNaziv() {return this.Naziv;}
}
