package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import hr.fer.wpu.b_ballteam.database.AppDatabase;

public class PlayerProfileActivity extends AppCompatActivity {

    int id;
    String oib;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile);

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        Intent intent = new Intent(this, PlayerProfileActivity.class);
        Bundle extras = getIntent().getExtras();
        id =  extras.getInt("id");
        oib = extras.getString("oib");

        String ime = database.getIgracDao().getIme(oib);
        String prezime = database.getIgracDao().getPrezime(oib);
        TextView imeiprezime = findViewById(R.id.imeiprezime);
        imeiprezime.setText(ime.toUpperCase() + " " + prezime.toUpperCase());

        //postavi broj na dresu
        Integer broj = database.getIgracDao().getBrojDresa(id);
        String brojAsString = String.valueOf(broj);
        TextView brojView = findViewById(R.id.broj);
        brojView.setText("Broj: " + brojAsString);

        //postaviti datum rodjenja
        String datumRodjenja = database.getIgracDao().getDatumRodjenja(id);
        TextView datumRodjendanView = findViewById(R.id.datumRodjenja);
        datumRodjendanView.setText("Datum rođenja: " + datumRodjenja);

        //postaviti poziciju
        String pozicija = database.getIgracDao().getPozicija(id);
        TextView pozicijaView = findViewById(R.id.pozicija);
        pozicijaView.setText("Pozicija: "+ pozicija);

        //postavi visinu
        String visina = database.getIgracDao().getVisina(id);
        TextView visinaView = findViewById(R.id.visina);
        visinaView.setText("Visina: " + visina);
    }
}
