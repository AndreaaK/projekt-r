package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MatchReviewActivity extends AppCompatActivity {

    Button btnPastGames;
    Button btnFutureGames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_review);

        btnPastGames = findViewById(R.id.pastGames);
        btnFutureGames = findViewById(R.id.futureGames);

        btnPastGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MatchReviewActivity.this, PastGamesActivity.class);
                startActivity(intent1);
            }
        });

        btnFutureGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MatchReviewActivity.this, FutureGamesActivity.class);
                startActivity(intent2);
            }
        });
    }
}