package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.models.Transakcija;

@Dao
public interface TransakcijaDao {
    @Query("SELECT * FROM Transakcija WHERE OIB = :OIB")
    List<Transakcija> getAllTransactionsFrom(String OIB);

    @Transaction
    @Insert
    void insertTransakcija(Transakcija transakcija);
}
