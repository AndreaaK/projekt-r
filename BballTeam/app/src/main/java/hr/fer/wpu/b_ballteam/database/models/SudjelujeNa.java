package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"OIB", "IDUtakmica"},
        foreignKeys = {@ForeignKey(entity = Osoba.class, parentColumns = "OIB", childColumns = "OIB", onDelete = CASCADE),
                @ForeignKey(entity = Utakmica.class, parentColumns = "IDUtakmica", childColumns = "IDUtakmica", onDelete = CASCADE)},
        indices = {@Index(value = "OIB"), @Index(value = "IDUtakmica")})
@Data
public class SudjelujeNa {
    @NonNull
    public String OIB;

    @NonNull
    public Long IDUtakmica;

    public SudjelujeNa(@NonNull String OIB, @NonNull Long IDUtakmica) {
        this.OIB = OIB;
        this.IDUtakmica = IDUtakmica;
    }
}
