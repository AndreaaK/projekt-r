package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.SudjelujeNa;

@Dao
public interface SudjelujeNaDao {
    @Insert
    void insertSudjelujeNa(SudjelujeNa sudjelujeNa);
}
