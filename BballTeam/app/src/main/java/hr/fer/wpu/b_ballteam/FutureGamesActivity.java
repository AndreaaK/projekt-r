package hr.fer.wpu.b_ballteam;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;
import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.GamesClass;

public class FutureGamesActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_future_games);

        listView = findViewById(R.id.futureGamesList);

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<GamesClass> pastGames = database.getUtakmicaDao().getFutureGames();
        Date novi = new Date();

        String mTeam1[] = new String[pastGames.size()];
        String mTeam2[] = new String[pastGames.size()];
        String mDate[] = new String[pastGames.size()];
        int images[] = new int[pastGames.size()];

        for(int i = 0; i < pastGames.size(); i++){

            mTeam1[i] = "B-ballTeam ";
            mTeam2[i] = pastGames.get(i).getNazivKluba();
            mDate[i] = pastGames.get(i).getPocetak().toString();

            images[i] = R.drawable.unknown_icon;
        }

        MyAdapter adapter = new MyAdapter(this, mTeam1, mTeam2, mDate, images);
        listView.setAdapter(adapter);

    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTeam1[];
        String rTeam2[];
        String rDate[];
        int rImgs[];

        MyAdapter(Context c, String team1[], String team2[], String date[], int imgs[]) {
            super(c, R.layout.pastgamesrow, R.id.tim1, team1);
            this.context = c;
            this.rTeam1 = team1;
            this.rTeam2 = team2;
            this.rDate = date;
            this.rImgs = imgs;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.pastgamesrow, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTeam1 = row.findViewById(R.id.tim1);
            TextView myTeam2 = row.findViewById(R.id.tim2);
            TextView myDate = row.findViewById(R.id.datum);

            images.setImageResource(rImgs[position]);
            myTeam1.setText(rTeam1[position]);
            myTeam2.setText(rTeam2[position]);
            myDate.setText(rDate[position]);

            return row;
        }
    }
}