package hr.fer.wpu.b_ballteam;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.PlanAktivnosti;

public class RasporedAktivnostiActivity extends AppCompatActivity {

    Integer id;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raspored_aktivnosti);

        Bundle extras = getIntent().getExtras();
        id =  extras.getInt("id");

        listView = findViewById(R.id.dan);

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<PlanAktivnosti> planAktivnosti = database.getPlanAktivnostiDao().getAllAktivnostiByKategorija(id);

        String mOpis[] = new String[planAktivnosti.size()];
        String mLokacija[] = new String[planAktivnosti.size()];

        //naci bolje rjesenje za ovo ruzno punjenje polja
        int images[] = new int[planAktivnosti.size()];

        int ids[] = new int[planAktivnosti.size()];
        int[] days = new int[planAktivnosti.size()];




        for(int i = 0; i < planAktivnosti.size(); i++){

            mOpis[i] = planAktivnosti.get(i).getSadrzaj();
            mLokacija[i] = planAktivnosti.get(i).getLokacija();
            images[i] = R.drawable.ponedjeljak;

            ids[i] = planAktivnosti.get(i).getIDPLanAktivnosti();

            if(planAktivnosti.get(i).getDan() == 0){
                images[i] = R.drawable.ponedjeljak;
            }else if(planAktivnosti.get(i).getDan() == 1){
                images[i] = R.drawable.utorak;
            }else if(planAktivnosti.get(i).getDan() == 2){
                images[i] = R.drawable.srijeda;
            }else if(planAktivnosti.get(i).getDan() == 3){
                images[i] = R.drawable.cetvrtak;
            }else if(planAktivnosti.get(i).getDan() == 4){
                images[i] = R.drawable.petak;
            }else if(planAktivnosti.get(i).getDan() == 5){
                images[i] = R.drawable.subota;
            }else if(planAktivnosti.get(i).getDan() == 6){
                images[i] = R.drawable.nedjelja;
            }
        }

        MyAdapter adapter = new MyAdapter(this, mOpis, mLokacija, images, ids);
        listView.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rOpis[];
        String rLokacija[];
        int rImgs[];
        int ids[];

        MyAdapter(Context c, String opis[], String lokacija[], int imgs[], int ids[]) {
            super(c, R.layout.activity_raspored_aktivnosti_row, R.id.opis, opis);
            this.context = c;
            this.rOpis = opis;
            this.rLokacija = lokacija;
            this.rImgs = imgs;
            this.ids = ids;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.activity_raspored_aktivnosti_row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView opis = row.findViewById(R.id.opis);
            TextView lokacija = row.findViewById(R.id.lokacija);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(RasporedAktivnostiActivity.this, DanDetaljiActivity.class);
                    Bundle extras = new Bundle();
                    extras.putInt("id", ids[position]);
                    extras.putString("Username", getIntent().getStringExtra("Username"));
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });

            images.setImageResource(rImgs[position]);
            opis.setText(rOpis[position]);
            lokacija.setText(rLokacija[position]);

            return row;
        }
    }
}