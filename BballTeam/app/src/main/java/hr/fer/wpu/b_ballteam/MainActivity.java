package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    Button btnPlayers;
    Button btnMatches;
    Button btnDailyActivity;
    Button btnInfo;
    Button btnFinancije;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlayers = (Button) findViewById(R.id.players);
        btnMatches = (Button) findViewById(R.id.matches);
        btnDailyActivity = (Button) findViewById(R.id.dailyActivity);
        btnInfo = (Button) findViewById(R.id.btnInfo);
        btnFinancije = (Button) findViewById(R.id.btnFinancije);

        btnPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
                startActivity(intent);
            }
        });
        btnMatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(MainActivity.this, MatchReviewActivity.class);
                startActivity(intent2);
            }
        });
        btnDailyActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this, DnevnikAktivnostiActivity.class);
                intent3.putExtra("Username", getIntent().getStringExtra("Username"));
                startActivity(intent3);
            }
        });
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent4 = new Intent(MainActivity.this, ClubInfoActivity.class);
                startActivity(intent4);
            }
        });
        btnFinancije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent5 = new Intent(MainActivity.this, UserFinanceActivity.class);
                intent5.putExtra("Username", getIntent().getStringExtra("Username"));
                startActivity(intent5);
            }
        });
    }
}