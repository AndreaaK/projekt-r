package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Osoba.class, parentColumns = "OIB", childColumns = "OIB", onDelete = CASCADE),
        indices = {@Index(value = "OIB")})
@Data
public class ZdrastveniDjelatnik {
    @PrimaryKey(autoGenerate = true)
    public Long IDZdrastveniDjelatnik;

    @NonNull
    public String OIB;

    public ZdrastveniDjelatnik(@NonNull String OIB) {
        this.OIB = OIB;
    }
}
