package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import hr.fer.wpu.b_ballteam.database.models.Tajnik;

@Dao
public interface TajnikDao {
    @Insert
    void insertTajnik(Tajnik tajnik);
}
