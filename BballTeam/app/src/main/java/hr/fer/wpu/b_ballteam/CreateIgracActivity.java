package hr.fer.wpu.b_ballteam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Igrac;
import hr.fer.wpu.b_ballteam.database.models.Osoba;

public class CreateIgracActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    AppDatabase database;

    EditText editVisina;
    EditText editTezina;
    EditText editBroj;
    EditText editDatum;

    Spinner spinnerPozicija;
    Spinner spinnerKategorija;

    Button btnStvori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_igrac);

        database = AppDatabase.getDatabase(getApplicationContext());

        editVisina = (EditText) findViewById(R.id.editVisinaIgraca);
        editTezina = (EditText) findViewById(R.id.editTezinaIgraca);
        editBroj = (EditText) findViewById(R.id.editBrojDresa);
        editDatum = (EditText) findViewById(R.id.editDatumRodjenja);

        spinnerPozicija = (Spinner) findViewById(R.id.spinnerPozicija);
        spinnerKategorija = (Spinner) findViewById(R.id.spinnerKategorija);

        btnStvori = (Button) findViewById(R.id.btnStvori);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.pozicije, R.layout.support_simple_spinner_dropdown_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPozicija.setAdapter(adapter1);
        spinnerPozicija.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.kategorija, R.layout.support_simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKategorija.setAdapter(adapter2);
        spinnerKategorija.setOnItemSelectedListener(this);

        btnStvori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pozicija = spinnerPozicija.getSelectedItem().toString();
                String kategorija = spinnerKategorija.getSelectedItem().toString();
                String visina = editVisina.getText().toString();
                String tezina = editTezina.getText().toString();
                String broj = editBroj.getText().toString();
                String datum = editDatum.getText().toString();

                String username = getIntent().getStringExtra("KI");
                String password = getIntent().getStringExtra("sifra");
                String oib = getIntent().getStringExtra("id");
                String ime = getIntent().getStringExtra("ime");
                String prezime = getIntent().getStringExtra("prezime");
                String role = getIntent().getStringExtra("uloga");

                try{
                    Integer vis = Integer.parseInt(visina);
                }catch(RuntimeException e){
                    Toast.makeText(CreateIgracActivity.this,
                            "Visina mora biti prirodan broj", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Integer.parseInt(visina) == 0){
                    Toast.makeText(CreateIgracActivity.this,
                            "Visina mora biti veća od 0", Toast.LENGTH_SHORT).show();
                    return;
                }

                try{
                    Integer tez = Integer.parseInt(tezina);
                }catch(RuntimeException e){
                    Toast.makeText(CreateIgracActivity.this,
                            "Tezina mora biti prirodan broj", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Integer.parseInt(tezina) == 0){
                    Toast.makeText(CreateIgracActivity.this,
                            "Težina mora biti veća od 0", Toast.LENGTH_SHORT).show();
                    return;
                }

                try{
                    Integer b = Integer.parseInt(broj);
                }catch(RuntimeException e){
                    Toast.makeText(CreateIgracActivity.this,
                            "Neispravan broj dresa", Toast.LENGTH_SHORT).show();
                    return;
                }
                try{
                    Igrac ig = database.getIgracDao().getIgracByBrojDresa(Integer.parseInt(broj), database.getKategorijaDao().getIDKategorije(kategorija));
                    if(ig == null){throw new NullPointerException();}
                    Toast.makeText(CreateIgracActivity.this,
                            "Zauzet broj dresa", Toast.LENGTH_SHORT).show();
                    return;
                }catch (NullPointerException e){}

                Osoba osoba = new Osoba(oib, ime, prezime, username, password, role);
                Igrac igrac = new Igrac(oib, datum, pozicija, visina, tezina, Integer.parseInt(broj),
                        database.getKategorijaDao().getIDKategorije(kategorija));
                database.getOsobaDao().insertOsoba(osoba);
                database.getIgracDao().insertIgrac(igrac);

                Toast.makeText(CreateIgracActivity.this,
                        "Unesen igrač", Toast.LENGTH_SHORT).show();

                //vrati se na pocetku
                Intent intent = new Intent(CreateIgracActivity.this, TajnikMainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}