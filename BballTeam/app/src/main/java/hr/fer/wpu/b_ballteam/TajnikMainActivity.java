package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class TajnikMainActivity extends AppCompatActivity {

    Button btnFinancije;
    Button btnKreirajRacun;
    Button btnPocetna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tajnik_main);

        btnFinancije = (Button) findViewById(R.id.financijeButton);
        btnKreirajRacun = (Button) findViewById(R.id.kreirajRacun);
        btnPocetna = (Button) findViewById(R.id.btnMain);

        btnFinancije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TajnikMainActivity.this, CheckFinanceActivity.class);
                startActivity(intent);
            }
        });
        btnKreirajRacun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(TajnikMainActivity.this, CreateAccountActivity.class);
                startActivity(intent2);
            }
        });
        btnPocetna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(TajnikMainActivity.this, MainActivity.class);
                intent3.putExtra("Username", getIntent().getStringExtra("Username"));
                startActivity(intent3);
            }
        });
    }
}