package hr.fer.wpu.b_ballteam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import hr.fer.wpu.b_ballteam.database.AppDatabase;

public class Coach_activity_plan extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    String sadrzaj;
    String lokacija;
    String detalji;
    String vrijemePocetka;
    EditText editVrijemePocetka;
    EditText editLokacija;
    EditText editDetalji;
    int id;
    Button home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_plan);

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        Intent intent = new Intent(this, DanDetaljiActivity.class);
        Bundle extras = getIntent().getExtras();
        id =  extras.getInt("id");

        //spinner isto kao create account
        Spinner spinner = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.vrsta_aktivnosti, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        Button urediPlanAktivnosti = findViewById(R.id.btnUrediPlanAktivnosti);

        sadrzaj = spinner.getSelectedItem().toString();
        editLokacija = findViewById(R.id.editLokacija);
        editDetalji = findViewById(R.id.editDetalji);
        editVrijemePocetka = findViewById(R.id.editvrijemePocetka);

        urediPlanAktivnosti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sadrzaj = spinner.getSelectedItem().toString();
                lokacija = editLokacija.getText().toString();
                vrijemePocetka = editVrijemePocetka.getText().toString();
                detalji = editDetalji.getText().toString();

                //updateaj bazu
                database.getPlanAktivnostiDao().updateDetalje(id,detalji);
                database.getPlanAktivnostiDao().updateLokacija(id, lokacija);
                database.getPlanAktivnostiDao().updateSadrzaj(id, sadrzaj);
                database.getPlanAktivnostiDao().updateVrijemePocetka(id, vrijemePocetka);

                //vrati se na pocetnu
                Intent intent = new Intent(Coach_activity_plan.this, DanDetaljiActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("id", id);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });



    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}