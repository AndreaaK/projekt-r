package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

import lombok.Data;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {@ForeignKey(entity = Osoba.class, parentColumns = "OIB", childColumns = "OIB", onDelete = CASCADE),
        @ForeignKey(entity = Kategorija.class, parentColumns = "IDKategorija", childColumns = "IDKategorija", onDelete = CASCADE)},
        indices = {@Index(value = "OIB"), @Index(value = "IDKategorija")})
@Data
public class Igrac {
    @PrimaryKey(autoGenerate = true)
    public Long IDIgrac;

    @ForeignKey(entity = Osoba.class, parentColumns = "OIB", childColumns = "OIB")
    @NonNull
    public String OIB;

    @NonNull
    public String DatumRodenja;

    @NonNull
    @Size(max = 20)
    public String Pozicija;

    @NonNull
    @Size(max = 5)
    public String Visina;

    @NonNull
    @Size(max = 5)
    public String Tezina;

    @NonNull
    public Integer Broj;

    @NonNull
    public Long IDKategorija;

    public Igrac() {

    }

    public Igrac(@NonNull String OIB, @NonNull String DatumRodenja, @NonNull String pozicija, @NonNull String visina,
                 @NonNull String tezina, @NonNull Integer Broj, @NonNull Long IDKategorija) {
        this.OIB = OIB;
        this.DatumRodenja = DatumRodenja;
        Pozicija = pozicija;
        Visina = visina;
        Tezina = tezina;
        this.Broj = Broj;
        this.IDKategorija = IDKategorija;
    }
}
