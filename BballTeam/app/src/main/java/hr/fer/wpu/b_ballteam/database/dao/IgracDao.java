package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import hr.fer.wpu.b_ballteam.database.models.Igrac;

@Dao
public interface IgracDao {

    /**
     * Funkcija za dohvat igraca po oibu
     * @param OIB
     * @return igrac
     */
    @Query("SELECT * FROM Igrac WHERE OIB = :OIB")
    Igrac getIgrac(String OIB);

    /**
     * Funkcija za dohvat Prezimena igraca po id-u igraca
     * @param OIB oib igraca
     * @return oib igraca
     */
    @Query("SELECT Prezime FROM Osoba WHERE OIB = :OIB")
    String getPrezime(String OIB);


    /**
     * Funkcija za dohvat Imena igraca po id-u igraca
     * @param OIB oib igraca
     * @return broj na dresu
     */
    @Query("SELECT Ime FROM Osoba WHERE OIB = :OIB")
    String getIme(String OIB);


    /**
     * Funkcija za dohvat datuma rodenja po id-u igraca.
     * @param IDIgrac id igraca
     * @return datum rodenja
     */
    @Query("SELECT DatumRodenja FROM Igrac WHERE IDIgrac = :IDIgrac")
    String getDatumRodjenja(int IDIgrac);


    /**
     * Funkcija koja vraca poziciju igraca po id-u igraca
     * @param IDIgrac id igraca
     * @return pozicija igraca
     */
    @Query("SELECT Pozicija FROM Igrac WHERE IDIgrac = :IDIgrac")
    String getPozicija(int IDIgrac);


    /**
     * Funkcija za dohvat visine igraca po id-u igraca
     * @param IDIgrac id igraca
     * @return visina igraca
     */
    @Query("SELECT Visina FROM Igrac WHERE IDIgrac = :IDIgrac")
    String getVisina(int IDIgrac);


    /**
     * Funkcija za dohvat broja dresa igraca po id-u igraca
     * @param IDIgrac id igraca
     * @return broj na dresu
     */
    @Query("SELECT Broj FROM Igrac WHERE IDIgrac = :IDIgrac")
    Integer getBrojDresa(int IDIgrac);

    @Query("SELECT * FROM Igrac WHERE Broj = :Broj AND IDKategorija =:kategorija ")
    Igrac getIgracByBrojDresa(int Broj, long kategorija);


    @Insert
    void insertIgrac(Igrac igrac);


}
