package hr.fer.wpu.b_ballteam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.io.File;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Igrac;
import hr.fer.wpu.b_ballteam.database.models.Kategorija;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.ProtivnickiKlub;
import hr.fer.wpu.b_ballteam.database.models.User;
import hr.fer.wpu.b_ballteam.database.models.Utakmica;

public class LoginActivity extends AppCompatActivity {
    AppDatabase dataBase;

    Button btnLogin;
    EditText txtUsername;
    EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        txtUsername = (EditText) findViewById(R.id.userName);
        txtPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.buttonLogin);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();

                //////////////////////////////////////////////////////////
                ////  Dodavanja početnih podataka u bazu podataka     ////
                ////  Pokrenuti samo jednom i onda opet zakomentirati ////
                //////////////////////////////////////////////////////////

              /*  dataBase.getInsertDataDao().insertOsoba();
                dataBase.getInsertDataDao().insertTrener();
                dataBase.getInsertDataDao().insertZdrastveniDjelatnik();
                dataBase.getInsertDataDao().insertTajnik();
                dataBase.getInsertDataDao().insertKategorija();
                dataBase.getInsertDataDao().insertIgrac();
                dataBase.getInsertDataDao().insertProtivnickiKlub();
                dataBase.getInsertDataDao().insertUtakmica();
                dataBase.getInsertDataDao().insertVrstaTransakcije();
                dataBase.getInsertDataDao().insertTransakcije();
                dataBase.getInsertDataDao().insertPlanAktivnosti();*/

                Osoba osoba = dataBase.getOsobaDao().login(userName, password);
                String role = null;
                try {
                    role = osoba.getUloga();
                } catch (NullPointerException e) {
                    role = "wrong";
                }

                if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("osoba")) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("admin")) {
                    Intent intent = new Intent(LoginActivity.this, TajnikMainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("tajnik")) {
                    Intent intent = new Intent(LoginActivity.this, TajnikMainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("trener")) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("zdravstveni")) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else if (userName.length() > 0 && password.length() > 0 && osoba != null && role.equals("igrac")) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Username", osoba.getKorisničkoIme());

                    Toast.makeText(LoginActivity.this, "Uspješna prijava", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Neuspješna prijava", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}