package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.models.VrstaTransakcije;

@Dao
public interface VrstaTransakcijeDao {
    @Query("SELECT * FROM VrstaTransakcije")
    List<VrstaTransakcije> getAllVrsteTransakcija();

    @Insert
    void insertVrstaTransakcije(VrstaTransakcije vrstaTransakcije);
}
