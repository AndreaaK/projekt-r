package hr.fer.wpu.b_ballteam;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Osoba;
import hr.fer.wpu.b_ballteam.database.models.Transakcija;
import hr.fer.wpu.b_ballteam.database.models.VrstaTransakcije;

public class CheckTransactionActivity extends AppCompatActivity {

    ListView listView;
    AppDatabase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_transaction);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        //Intent in = getIntent();
        //Bundle bundle = in.getBundleExtra("MyPackage");
        String userName = getIntent().getStringExtra("Username");

        Osoba osoba = dataBase.getOsobaDao().getOsobaByUsername(userName);
        String OIB = osoba.getOIB();

        AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
        List<Transakcija> Transakcije = database.getTransakcijaDao().getAllTransactionsFrom(OIB);

        String iznos[] = new String[Transakcije.size()];
        String datum[] = new String[Transakcije.size()];

        for (int i = 0; i < Transakcije.size(); i++) {
            Long vrsta = Transakcije.get(i).getIDVrstaTransakcija();
            String ispis = String.format("%.2f", Transakcije.get(i).getIznos());
            String znak;
            if (vrsta == (long)1){znak = "+";}
            else{znak = "-";}
            iznos[i] = znak + ispis;
            datum[i] = Transakcije.get(i).getDatumTransakcije();
        }

        listView = findViewById(R.id.TransakcijeFromOIB);

        CheckTransactionActivity.MyAdapter adapter = new CheckTransactionActivity.MyAdapter(this, iznos, datum);
        listView.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String iznos[];
        String datum[];

        MyAdapter(Context c, String iznos[], String datum[]) {
            super(c, R.layout.row, R.id.imePrezime, iznos);
            this.context = c;
            this.iznos = iznos;
            this.datum = datum;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.imePrezime);
            TextView myDescription = row.findViewById(R.id.opis);
            /*row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(CheckTransactionActivity.this, PlayerProfileActivity.class);
                    startActivity(intent);
                }
            });*/

            images.setImageResource(R.mipmap.ic_launcher);
            myTitle.setText(iznos[position]);
            myDescription.setText(datum[position].toString());

            return row;
        }
    }
}