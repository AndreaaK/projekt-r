package hr.fer.wpu.b_ballteam.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import hr.fer.wpu.b_ballteam.database.models.PlanAktivnosti;

@Dao
public interface PlanAktivnostiDao {
    @Insert
    void insertPlanAktivnosti(PlanAktivnosti planAktivnosti);

    @Query("SELECT * FROM PlanAktivnosti WHERE IDKategorija = :kategorijaID")
    List<PlanAktivnosti> getAllAktivnostiByKategorija(Integer kategorijaID);

    @Query("SELECT Sadrzaj FROM PlanAktivnosti WHERE IDPLanAktivnosti = :id")
    String getSadrzaj(int id);


    @Query("SELECT Detalj FROM PlanAktivnosti WHERE IDPLanAktivnosti = :id")
    String getDetalji(int id);

    @Query("SELECT Lokacija FROM PlanAktivnosti WHERE IDPLanAktivnosti = :id")
    String getLokacija(int id);

    @Query("SELECT VrijemePočetka FROM PlanAktivnosti WHERE IDPLanAktivnosti = :id")
    String getVrijemePocetak(int id);


    @Query("UPDATE PlanAktivnosti SET VrijemePočetka = :pocetak WHERE IDPLanAktivnosti = :id")
    void updateVrijemePocetka(int id, String pocetak);

    @Query("UPDATE PlanAktivnosti SET Detalj = :detalj WHERE IDPLanAktivnosti = :id")
    void updateDetalje(int id, String detalj);

    @Query("UPDATE PlanAktivnosti SET Sadrzaj = :sadrzaj WHERE IDPLanAktivnosti = :id")
    void updateSadrzaj(int id, String sadrzaj);

    @Query("UPDATE PlanAktivnosti SET Lokacija = :lokacija WHERE IDPLanAktivnosti = :id")
    void updateLokacija(int id, String lokacija);




}
