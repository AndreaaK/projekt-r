package hr.fer.wpu.b_ballteam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import hr.fer.wpu.b_ballteam.database.AppDatabase;
import hr.fer.wpu.b_ballteam.database.models.Transakcija;

public class NewTransactionActivity extends AppCompatActivity {

    AutoCompleteTextView editOIB;
    EditText editIznos;
    AppDatabase dataBase;

    Button btnUplatiKorisniku;
    Button btnNaplatiKorisniku;
    Button btnStaviNaRacun;
    Button btnSkiniSRacuna;

    String[] osobe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_transaction);

        editIznos = (EditText) findViewById(R.id.editIznos);
        editOIB = findViewById(R.id.editOIB2);

        dataBase = AppDatabase.getDatabase(getApplicationContext());

        osobe = dataBase.getOsobaDao().getAllOsoba();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, osobe);

        editOIB.setAdapter(adapter);

        btnUplatiKorisniku = (Button) findViewById(R.id.btnUplatiKorisniku);
        btnNaplatiKorisniku = (Button) findViewById(R.id.btnNaplatiKorisniku);
        btnStaviNaRacun = (Button) findViewById(R.id.btnStaviNaRacun);
        btnSkiniSRacuna = (Button) findViewById(R.id.btnSkiniSRacun);

        btnUplatiKorisniku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedText = editOIB.getText().toString();
                String[] oib = selectedText.split(", OIB: ");
                Double svota;

                try{
                    if(oib[1].length() != 11){
                        Toast.makeText(NewTransactionActivity.this, "Neispravan OIB", Toast.LENGTH_SHORT).show();
                    }
                    else if(!dataBase.getOsobaDao().CheckOsoba(oib[1]).equals(oib[0])){
                        Toast.makeText(NewTransactionActivity.this, "Upisani OIB ne pripada upisanoj osobi", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        try{
                            svota = Double.parseDouble(editIznos.getText().toString());
                        }catch (RuntimeException e){
                            Toast.makeText(NewTransactionActivity.this, "Iznos mora imati dvije decimale", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Date currentTime = Calendar.getInstance().getTime();
                        String datum = currentTime.toString();

                        Transakcija transakcijaClanu = new Transakcija(svota, datum, oib[1], (long)1);
                        Transakcija transakcijaKlubu = new Transakcija(svota, datum, "98765432100", (long)2);

                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaClanu);
                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaKlubu);

                        Toast.makeText(NewTransactionActivity.this,"Transakcija izvršena!", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex){
                    Toast.makeText(NewTransactionActivity.this, "Neispravan unos podataka", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnNaplatiKorisniku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedText = editOIB.getText().toString();
                String[] oib = selectedText.split(", OIB: ");
                Double svota;

                try{
                    if(oib[1].length() != 11){
                        Toast.makeText(NewTransactionActivity.this, "Neispravan OIB", Toast.LENGTH_SHORT).show();
                    }
                    else if(!dataBase.getOsobaDao().CheckOsoba(oib[1]).equals(oib[0])){
                        Toast.makeText(NewTransactionActivity.this, "Upisani OIB ne pripada upisanoj osobi", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        try{
                            svota = Double.parseDouble(editIznos.getText().toString());
                        }catch (RuntimeException e){
                            Toast.makeText(NewTransactionActivity.this, "Iznos mora imati dvije decimale", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Date currentTime = Calendar.getInstance().getTime();
                        String datum = currentTime.toString();

                        Transakcija transakcijaClanu = new Transakcija(svota, datum, oib[1], (long)2);
                        Transakcija transakcijaKlubu = new Transakcija(svota, datum, "98765432100", (long)1);

                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaClanu);
                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaKlubu);

                        Toast.makeText(NewTransactionActivity.this,"Transakcija izvršena!", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex){
                    Toast.makeText(NewTransactionActivity.this, "Neispravan unos podataka", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnStaviNaRacun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedText = editOIB.getText().toString();
                String[] oib = selectedText.split(", OIB: ");
                Double svota;

                try{
                    if(oib[1].length() != 11){
                        Toast.makeText(NewTransactionActivity.this, "Neispravan OIB", Toast.LENGTH_SHORT).show();
                    }
                    else if(!dataBase.getOsobaDao().CheckOsoba(oib[1]).equals(oib[0])){
                        Toast.makeText(NewTransactionActivity.this, "Upisani OIB ne pripada upisanoj osobi", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        try{
                            svota = Double.parseDouble(editIznos.getText().toString());
                        }catch (RuntimeException e){
                            Toast.makeText(NewTransactionActivity.this, "Iznos mora imati dvije decimale", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Date currentTime = Calendar.getInstance().getTime();
                        String datum = currentTime.toString();

                        Transakcija transakcijaOsobi = new Transakcija(svota, datum, oib[1], (long)1);

                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaOsobi);

                        Toast.makeText(NewTransactionActivity.this,"Transakcija izvršena!", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex){
                    Toast.makeText(NewTransactionActivity.this, "Neispravan unos podataka", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSkiniSRacuna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedText = editOIB.getText().toString();
                String[] oib = selectedText.split(", OIB: ");
                Double svota;

                try{
                    if(oib[1].length() != 11){
                        Toast.makeText(NewTransactionActivity.this, "Neispravan OIB", Toast.LENGTH_SHORT).show();
                    }
                    else if(!dataBase.getOsobaDao().CheckOsoba(oib[1]).equals(oib[0])){
                        Toast.makeText(NewTransactionActivity.this, "Upisani OIB ne pripada upisanoj osobi", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        try{
                            svota = Double.parseDouble(editIznos.getText().toString());
                        }catch (RuntimeException e){
                            Toast.makeText(NewTransactionActivity.this, "Iznos mora imati dvije decimale", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Date currentTime = Calendar.getInstance().getTime();
                        String datum = currentTime.toString();

                        Transakcija transakcijaOsobi = new Transakcija(svota, datum, oib[1], (long)2);

                        dataBase.getTransakcijaDao().insertTransakcija(transakcijaOsobi);

                        Toast.makeText(NewTransactionActivity.this,"Transakcija izvršena!", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex){
                    Toast.makeText(NewTransactionActivity.this, "Neispravan unos podataka", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}