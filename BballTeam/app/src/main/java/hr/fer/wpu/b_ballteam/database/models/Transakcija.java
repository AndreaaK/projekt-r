package hr.fer.wpu.b_ballteam.database.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import hr.fer.wpu.b_ballteam.converter.DateConverter;
import lombok.Data;
import lombok.Generated;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = {@ForeignKey(entity = Osoba.class, parentColumns = "OIB", childColumns = "OIB", onDelete = CASCADE),
                @ForeignKey(entity = VrstaTransakcije.class, parentColumns = "IDVrstaTransakcija", childColumns = "IDVrstaTransakcija", onDelete = CASCADE)},
        indices = {@Index(value = "OIB"), @Index(value = "IDVrstaTransakcija")})
@Data
public class Transakcija {
    @PrimaryKey(autoGenerate = true)
    public Long IDTransakcija;

    @NonNull
    public Double Iznos;

    @NonNull
    public String DatumTransakcije;

    @NonNull
    public String OIB;

    @NonNull
    public Long IDVrstaTransakcija;

    public Transakcija() {

    }

    public Transakcija(@NonNull Double iznos, @NonNull String datumTransakcije, @NonNull String OIB, Long IDVrstaTransakcija) {
        Iznos = iznos;
        DatumTransakcije = datumTransakcije;
        this.OIB = OIB;
        this.IDVrstaTransakcija = IDVrstaTransakcija;
    }
}
